<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         $this->setDataBaseStringLength();
    }

    /**
     *
    //تعیین طول کارکتر های مورد استفاده برای رفع مشکل ایجاد دیتا بیس در mysql
     */
    public function setDataBaseStringLength(): void
    {
        Schema::defaultStringLength(191);
    }
}
